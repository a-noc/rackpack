# RackPack

The RackPack is envisioned to be a solution, that can provide small to medium sized network infrastructure in the size of a backpack when disassembled for political events like protest camps, festivals, meetings and the like.

This infrastructure often is necessary for such events for several reasons. Some of these include:
- Internet connectivity for Info Points, in order to get current info, send out press releases and communicate with the "outside" world
- Print services for the Info Points
- Chat services (both on-site and off-site) for organizing the event and activities
- News board
- Internet connectivity for participants

Due to their nature, events of this kind have specific demands, that need to be met by such a solution:
- easily deployable: since such events normally take place without proper setup time, the network needs to be rolled out pretty quickly
- light-weight/flexible: most of the times at such events you don't necessarily have the option to bring bulky equipment and need to be flexible instead, as the circumstances might be subject to frequent changes
- low-budget: the equipment you use might face several risks, such as water (rain, water canon), force (cops, transport) or theft. Therefore the assets should be easy to replace, without causing financial catastrophes
- low energy consumption: as the electricity often is provided by a limited number of generators, the hardware's power consumption should respect the corresponding limits
- unknown demand: the requirements might be unclear until you start, so the hardware needs to cover a wide range of scenarios from multiple small setups up to one medium sized network
- secure: as we are talking about political activism, the solution needs to make sure that nobody can be identified as a participant by using the services offered

RackPack tries to meet these requirements by providing a centrally managed solution, which fits into a big backpack, when disassembled.

You can find out more about the concepts behind this solution, if you have a look at our [wiki](https://0xacab.org/a-noc/rackpack/wikis/home).

# Contact

If you would like to contact us, simply write an e-mail to [rackpack@nadir.org](mailto:rackpack@nadir.org).

This mailing list is GPG-enabled; you receive the public key, once you
- send an empty e-mail to [rackpack-sendkey@nadir.org](mailto:rackpack-sendkey@nadir.org)
- search the key servers
- grab our GPG key from [here](https://0xacab.org/a-noc/rackpack/raw/master/0x7EE4F89E.asc)

Fingerprint: 3237 CE74 4D49 F80E 6A86 0270 065B 16E9 7EE4 F89E